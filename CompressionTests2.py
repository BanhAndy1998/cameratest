import cv2 as cv
import time
import subprocess
from PIL import Image
from subprocess import Popen, PIPE

cam = cv.VideoCapture(0)

cv.namedWindow("test")

img_counter = 0
prevFrame = 0
prevPing = 0
frame_rate = 2
prev = 0
successfulPings = 0
droppedPings = 0
picQuality = 50
pingInterval = 0.2
qualitySetting = False #If true, prioritize picture quality when adjusting for packet Loss
framerateSetting = False #If true, prioritize frame rate when adjusting for packet Loss

hostname = "192.168.191.171"


while True:

    #Grab a frame from the camera based on the frame rate

    frameTime_elapsed = time.time() - prevFrame
    ret, frame = cam.read()
    if frameTime_elapsed > 1./frame_rate:
        prevFrame = time.time()
        if not ret:
            print("failed to grab frame")
            break
        cv.imshow("test", frame)
        #Convert frame captured from camera to image file
        img_name = "opencv_frame_{}.JPG".format(img_counter)
        cv.imwrite(img_name, frame)
        image = Image.open("opencv_frame_{}.JPG".format(img_counter))
        convertedImage = image.convert('RGB').save("COMPopencv_frame_{}.JPG".format(img_counter),optimize=True,quality=picQuality)
        img_counter += 1

    #Check current connection status / packet loss here then adjust settings accordingly
    #Probably have to make this part into a new thread
    hostname = "192.168.191.171" 
    pingTime_elapsed = time.time() - prevPing
    if pingTime_elapsed > pingInterval:
        process = subprocess.Popen(['ping', '-c', '1', hostname], stdout=subprocess.PIPE)
        #if process.returncode == 0 :
            #successfulPings += 1
        #elif process.returncode == 2 :
           # droppedPings += 1

        #Using arbitrary numbers here for now. We can adjust number of pictures sent instead of dropping picture quality also
        if successfulPings + droppedPings > 24:
            totalSuccessfulPings = successfulPings - droppedPings
            if(qualitySetting):
                if(totalSuccessfulPings == 25 and picQuality == 100 and frame_rate < 60):
                    print("Framerate increased")
                    if(frame_rate >= 55):
                        frame_rate = 60
                    else:
                        frame_rate += 5
                elif(totalSuccessfulPings == 25 and picQuality < 100):
                    print("Picture quality increased")
                    if(picQuality >= 90):
                        picQuality == 100
                    else:
                        picQuality += 10
                elif(totalSuccessfulPings == 24):
                    print("No Adjustment")
                elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and frame_rate > 20):
                    print("Framerate decreased")
                    frame_rate -= 2
                elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and picQuality >= 20):
                    print("Picture quality decreased")
                    picQuality -= 10
                elif(totalSuccessfulPings <= 19 and frame_rate >= 20):
                    print("Framerate heavily decreased")
                    frame_rate -= 5
                elif(totalSuccessfulPings <= 19 and picQuality >= 40):
                    print("Picture quality heavily decreased")
                    picQuality -= 30

            elif(framerateSetting):
                if(totalSuccessfulPings == 25 and frame_rate == 60 and picQuality < 100):
                    print("Picture quality increased")
                    if(picQuality >= 90):
                        picQuality = 100
                    else:
                        picQuality += 10
                if(totalSuccessfulPings == 25 and frame_rate < 60):
                    print("Frame Rate increased")
                    if(frame_rate >= 55):
                        frame_rate = 60
                    else:
                        frame_rate += 5
                elif(totalSuccessfulPings == 24):
                    print("No Adjustment")
                elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and picQuality >= 20):
                    print("Picture quality decreased")
                    picQuality -= 10
                elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and frame_rate > 20):
                    print("Framerate decreased")
                    frame_rate -= 2
                elif(totalSuccessfulPings <= 19 and picQuality >= 40):
                    print("Picture quality heavily decreased")
                    picQuality -= 30
                elif(totalSuccessfulPings <= 19 and frame_rate >= 20):
                    print("Framerate heavily decreased")
                    frame_rate -= 5

            else:
                if(totalSuccessfulPings == 25 and (picQuality <= 100 or frame_rate <= 60)):
                    print("Stream quality increased")
                    if (picQuality >= 95 and picQuality < 100):
                        picQuality == 100
                    else:
                        picQuality += 5
                    if (frame_rate >= 58 and frame_rate < 60):
                        frame_rate == 60
                    else:
                        frame_rate += 2
                elif(totalSuccessfulPings == 24):
                    print("No Adjustment")
                elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23):
                    print("Stream quality decreased")
                    if(picQuality > 10 and picQuality <= 15):
                        picQuality = 10
                    else:
                        picQuality -= 5
                    if(frame_rate > 20 and frame_rate <= 22):
                        frame_rate = 20
                    else:
                        frame_rate -= 2
                elif(totalSuccessfulPings <= 19):
                    print("Stream quality heavily decreased")
                    if(picQuality > 10 and picQuality <= 30):
                        picQuality = 10
                    else:
                        picQuality -= 20
                    if(frame_rate > 20 and frame_rate <= 25):
                        frame_rate = 20
                    else:
                        frame_rate -= 5
            successfulPings = 0
            droppedPings = 0

    k = cv.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break    
    elif k%256 == 91:
        # [ pressed, simulates successful ping
        successfulPings += 1
        print("Total: " + str(successfulPings + droppedPings) + " | Successful: " + str(successfulPings) + " | Dropped: " + str(droppedPings))
    elif k%256 == 93:
        # ] pressed, simulates successful ping
        droppedPings += 1
        print("Total: " + str(successfulPings + droppedPings) + " | Successful: " + str(successfulPings) + " | Dropped: " + str(droppedPings))

cam.release()

cv.destroyAllWindows()
