import keyboard

frame_rate = 2
successfulPings = 0
droppedPings = 0
picQuality = 50
notPressed_success = True
notPressed_fail = True
qualitySetting = False #If true, prioritize picture quality when adjusting for packet Loss
framerateSetting = False #If true, prioritize frame rate when adjusting for packet Loss

while True:

    input = keyboard.read_key()

    if input == "esc":
        break    
    elif input == "[":
        # [ pressed, simulates successful ping
        if(notPressed_success):
            notPressed_success = False
            successfulPings += 1
            print("Total: " + str(successfulPings + droppedPings) + " | Successful: " + str(successfulPings) + " | Dropped: " + str(droppedPings))
        else:
            notPressed_success = True
    elif input == "]":
        # ] pressed, simulates successful ping
        if(notPressed_fail):
            notPressed_fail = False
            droppedPings += 1
            print("Total: " + str(successfulPings + droppedPings) + " | Successful: " + str(successfulPings) + " | Dropped: " + str(droppedPings))
        else:
            notPressed_fail = True

    #Using arbitrary numbers here for now. We can adjust number of pictures sent instead of dropping picture quality also
    if successfulPings + droppedPings > 24:
        totalSuccessfulPings = successfulPings - droppedPings
        if(qualitySetting):
            if(totalSuccessfulPings == 25 and picQuality == 100 and frame_rate < 60):
                print("Framerate increased")
                if(frame_rate >= 55):
                    frame_rate = 60
                else:
                    frame_rate += 5
            elif(totalSuccessfulPings == 25 and picQuality < 100):
                print("Picture quality increased")
                if(picQuality >= 90):
                    picQuality == 100
                else:
                    picQuality += 10
            elif(totalSuccessfulPings == 24):
                print("No Adjustment")
            elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and frame_rate > 20):
                print("Framerate decreased")
                frame_rate -= 2
            elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and picQuality >= 20):
                print("Picture quality decreased")
                picQuality -= 10
            elif(totalSuccessfulPings <= 19 and frame_rate >= 20):
                print("Framerate heavily decreased")
                frame_rate -= 5
            elif(totalSuccessfulPings <= 19 and picQuality >= 40):
                print("Picture quality heavily decreased")
                picQuality -= 30

        elif(framerateSetting):
            if(totalSuccessfulPings == 25 and frame_rate == 60 and picQuality < 100):
                print("Picture quality increased")
                if(picQuality >= 90):
                    picQuality = 100
                else:
                    picQuality += 10
            if(totalSuccessfulPings == 25 and frame_rate < 60):
                print("Frame Rate increased")
                if(frame_rate >= 55):
                    frame_rate = 60
                else:
                    frame_rate += 5
            elif(totalSuccessfulPings == 24):
                print("No Adjustment")
            elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and picQuality >= 20):
                print("Picture quality decreased")
                picQuality -= 10
            elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23 and frame_rate > 20):
                print("Framerate decreased")
                frame_rate -= 2
            elif(totalSuccessfulPings <= 19 and picQuality >= 40):
                print("Picture quality heavily decreased")
                picQuality -= 30
            elif(totalSuccessfulPings <= 19 and frame_rate >= 20):
                print("Framerate heavily decreased")
                frame_rate -= 5

        else:
            if(totalSuccessfulPings == 25 and (picQuality < 100 or frame_rate < 60)):
                print("Stream quality increased")
                if (picQuality >= 95 and picQuality < 100):
                    picQuality = 100
                else:
                    picQuality += 5
                if (frame_rate >= 58 and frame_rate < 60):
                    frame_rate = 60
                else:
                    frame_rate += 2
            elif(totalSuccessfulPings == 24):
                print("No Adjustment")
            elif(totalSuccessfulPings >= 20 and totalSuccessfulPings <= 23):
                print("Stream quality decreased")
                if(picQuality > 10 and picQuality <= 15):
                    picQuality = 10
                else:
                    picQuality -= 5
                if(frame_rate > 20 and frame_rate <= 22):
                    frame_rate = 20
                else:
                    frame_rate -= 2
            elif(totalSuccessfulPings <= 19):
                print("Stream quality heavily decreased")
                if(picQuality > 10 and picQuality <= 30):
                    picQuality = 10
                else:
                    picQuality -= 20
                if(frame_rate > 20 and frame_rate <= 25):
                    frame_rate = 20
                else:
                    frame_rate -= 5
        successfulPings = 0
        droppedPings = 0

